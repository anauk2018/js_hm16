const THEME_RED = 'red'
const THEME_BLACK = 'black'

const changeStyle = () => {
  const theme = localStorage.getItem('theme') || THEME_BLACK // Если раньше там ничего не было, считаем что по дефолту там пока черная тема
  localStorage.setItem('theme', theme === THEME_BLACK ? THEME_RED : THEME_BLACK) // Меняем тему в localStorage на противоположную

  if (theme === THEME_BLACK) {
    // установить нужные стили для всех нужных элементов, например черный цвет для document.body.style.background и т.д.
      document.body.style.background = "black";
      
  } else if (theme === THEME_RED) {
    // установить нужные стили для нужных элементов
      document.body.style.background = "red";
  }
}

const changeStyleButton = document.getElementById('change-style');
changeStyleButton.addEventListener('click', changeStyle);
